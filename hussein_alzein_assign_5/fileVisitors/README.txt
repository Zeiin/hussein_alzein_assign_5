Hussein Alzein
halzein1@binghamton.edu 

## To clean:
ant clean (in src folder)

-----------------------------------------------------------------------
## To compile: 
ant (in src folder)

-----------------------------------------------------------------------
## To run: (in src folder)
ant run -Darg0={INPUT_FILE} -Darg1={OUTPUT_FILE} -Darg2={DEBUG_VALUE} 

-----------------------------------------------------------------------
"I have done this assignment completely on my own. I have not copied
it, nor have I given my solution to anyone else. I understand that if
I am involved in plagiarism or cheating I will have to sign an
official form that I have cheated and that this form will be stored in
my official university record. I also understand that I will receive a
grade of 0 for the involved assignment for my first offense and that I
will receive a grade of F for the course for any additional
offense.”

[Date: 11/25/17]
I reused code I wrote during the group project for the Binary tree. Basically used the same NodeTree.java file to clarify.
-----------------------------------------------------------------------
Provide justification for Data Structures used in this assignment in
term of Big O complexity (time and/or space)

Used a binary search tree for the sake of print and insertion.
Palindrom checking and Prime length checking both check every node in the tree any way and order never matters 
so they were not considered.


-----------------------------------------------------------------------
DEBUG_VALUE 1 = Print to stdout everytime the tree is made
DEBUG_VALUE 2 = Print to stdout everytime a word is added
